package org.wiwat.papakhung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrationProcessServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrationProcessServiceApplication.class, args);
	}
}
