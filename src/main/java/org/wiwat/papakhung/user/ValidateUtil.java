package org.wiwat.papakhung.user;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ValidateUtil {
	public static String getMemberTypeName(Double salary) {
		String memberTypeName = null;
		if(salary.compareTo(new Double(50000))>0) {
			memberTypeName = "Platinum";
		}else if(salary.compareTo(new Double(30000))>0) {
			memberTypeName = "Gold";
		}else if(salary.compareTo(new Double(15000))>0) {
			memberTypeName = "Silver";
		}
		return memberTypeName;
		
	}
	public static String getReferenceCode(String phone) {
		String referenceCode = null;
		DateFormat formatter = new SimpleDateFormat("yyyymmDD");
		Date currentDate = new Date();
		String lst4str = phone.substring(phone.length() - 4, phone.length());
		referenceCode = formatter.format(currentDate)+lst4str;
		return referenceCode;
		
	}
}
