package org.wiwat.papakhung.user;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User {
	@Id
	@GeneratedValue
	private Long userId;
	private String username;
	private String password;
	private String address;
	private String phone;
	private String referenceCode ;
	private String memberType ;
	private Double salary;
	
	public User() {
		super();
	}

	public User(Long userId, String username, String password, String address, String phone, String referenceCode, Double salary) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.address = address;
		this.phone = phone;
		this.referenceCode = referenceCode;
		this.memberType = memberType;
		this.salary = salary;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

		
}
