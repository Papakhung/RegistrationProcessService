package org.wiwat.papakhung.user;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class UserResource {

	@Autowired
	private UserRepository UserRepository;

	@GetMapping("/Users")
	public List<User> retrieveAllUsers() {
		return UserRepository.findAll();
	}

	@GetMapping("/Users/{id}")
	public User retrieveUser(@PathVariable long id) {
		Optional<User> User = UserRepository.findById(id);

		if (!User.isPresent())
			throw new UserNotFoundException("id-" + id);

		return User.get();
	}

	@DeleteMapping("/Users/{userId}")
	public void deleteUser(@PathVariable long id) {
		UserRepository.deleteById(id);
	}

	@PostMapping("/Users")
	public ResponseEntity<Object> createUser(@RequestBody User user) {
		String memberTypeName = ValidateUtil.getMemberTypeName(user.getSalary());
		if (memberTypeName != null) {

			User savedUser = UserRepository.save(user);

			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(savedUser.getUserId()).toUri();
			return ResponseEntity.created(location).build();
		}else {
			return ResponseEntity
		            .status(HttpStatus.FORBIDDEN)
		            .body("Error Message");
		}
	}

	@PutMapping("/Users/{userId}")
	public ResponseEntity<Object> updateUser(@RequestBody User User, @PathVariable long userId) {

		Optional<User> UserOptional = UserRepository.findById(userId);

		if (!UserOptional.isPresent())
			return ResponseEntity.notFound().build();

		User.setUserId(userId);

		UserRepository.save(User);

		return ResponseEntity.noContent().build();
	}
}
